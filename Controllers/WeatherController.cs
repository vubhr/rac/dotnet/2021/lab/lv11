using Microsoft.AspNetCore.Mvc;

namespace WeatherApi.Controllers;

[Route("api/[controller]")]
public class WeatherController : ControllerBase {
  private readonly IWeatherService weatherService;

  public WeatherController(IWeatherService weatherService) {
    this.weatherService = weatherService;
  }

  [HttpGet]
  public IActionResult Get() {
    return Ok(weatherService.GetWeather());
  }

  [HttpGet("{city}")]
  public IActionResult Get(string city) {
    WeatherData? weather;
    if ((weather = weatherService.GetWeatherForCity(city)) != null) {
      return Ok(weather);
    } else {
      return NotFound(new { Error = "City not found in database" });
    }
  }
}
