namespace WeatherApi.Models;

public interface IWeatherService {
  IEnumerable<WeatherData> GetWeather();
  WeatherData? GetWeatherForCity(string city);
}