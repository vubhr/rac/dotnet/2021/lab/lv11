using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeatherApi.Models;

public class WeatherData {
  public WeatherData(string city, string temperature, string humidity) {
    City = city;
    Temperature = decimal.Parse(temperature);
    Humidity = int.Parse(humidity);
  }

  public string City { get; set; }
  public decimal Temperature { get; set; }
  public int Humidity { get; set; }
}
