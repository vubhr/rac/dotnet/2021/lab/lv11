using System.Xml;

namespace WeatherApi.Models;

public class WeatherService : IWeatherService {
  public WeatherService() {
    data = GetData();
  }

  public List<WeatherData> GetData() {
    XmlDocument document = new XmlDocument();
    document.Load("http://vrijeme.hr/hrvatska_n.xml");

    List<WeatherData> data = new List<WeatherData>();

    XmlNodeList citiesData = document.GetElementsByTagName("Grad");
    foreach (XmlNode cityData in citiesData) {
      data.Add(new WeatherData(
          cityData["GradIme"].InnerText,
          cityData["Podatci"]["Temp"]?.InnerText,
          cityData["Podatci"]["Vlaga"]?.InnerText
      ));
    }

    return data;
  }

  public IEnumerable<WeatherData> GetWeather() {
    return
      from d in data
      orderby d.City
      select d;
  }

  public WeatherData? GetWeatherForCity(string city) {
    var weather =
      (from d in data
      where d.City == city
      select d).ToList();

    if (weather.Count == 1) {
      return weather.First();
    } else {
      return null;
    }
  }

  private List<WeatherData> data;
}
